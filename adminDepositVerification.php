<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Deposit.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$depositDetails = getDeposit($conn," WHERE status = 'PENDING' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <?php //include 'meta.php'; ?> -->
    <meta property="og:url" content="https://dxforextrade88.com/adminDepositVerification.php" />
    <meta property="og:title" content="Deposit Request | De Xin Guo Ji 德鑫国际" />
    <title>Deposit Request | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminDepositVerification.php" />
	<!-- <?php //include 'css.php'; ?> -->
</head>
<body class="body">

<?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!-- <?php //include 'adminSidebar.php'; ?> -->

<div class="yellow-body padding-from-menu same-padding">

        <h1 class="details-h1" onclick="goBack()">
            <a class="black-white-link2 hover1">
                <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
                <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
                Deposit Number : #<?php echo $_POST['deposit_id'];?>
            </a>
        </h1>

    <!-- <form method="POST" id="paymentVerifiedForm" onsubmit="doPreview(this.submited); return false;"> -->
    <form method="POST" id="depositVerifiedForm" onsubmit="doPreview(this.submited); return false;">

        <div class="width100 shipping-div2">
            <table class="details-table">
                <tbody>
                <?php
                if(isset($_POST['deposit_id']))
                {
                    $conn = connDB();
                    //Order
                    // $orderArray = getDeposit($conn,"WHERE id = ? ", array("id") ,array($_POST['deposit_id']),"i");
                    $depositArray = getDeposit($conn,"WHERE id = ? ", array("id") ,array($_POST['deposit_id']),"i");

                    if($depositArray != null)
                    {
                        ?>
                        <tr>
                            <td>Userame</td>
                            <td>:</td>
                            <td><?php echo $depositArray[0]->getUsername()?></td>
                        </tr>
                        <tr>
                            <td>Bank Name</td>
                            <td>:</td>
                            <td><?php echo $depositArray[0]->getBankName()?></td>
                        </tr>
                        <tr>
                            <td>Deposit Amount</td>
                            <td>:</td>
                            <td><?php echo $depositArray[0]->getAmount()?></td>
                        </tr>
                        <tr>
                            <td>Reference</td>
                            <td>:</td>
                            <td><?php echo $depositArray[0]->getReference()?></td>
                        </tr>
                        <tr>
                            <td>Payment Date and Time</td>
                            <td>:</td>
                            <td><?php echo $depositArray[0]->getSubmitDate();?>&nbsp;<?php echo $depositArray[0]->getSubmitTime()?></td>
                        </tr>
                    <?php
                    }
                }?>
                </tbody>
            </table>
        </div>

        <input type="hidden" id="deposit_id" name="deposit_id" value="<?php echo $depositArray[0]->getId()?>">
        <input type="hidden" id="deposit_uid" name="deposit_uid" value="<?php echo $depositArray[0]->getUid()?>">
        <input type="hidden" id="deposit_username" name="deposit_username" value="<?php echo $depositArray[0]->getUsername()?>">
        <input type="hidden" id="deposit_amount" name="deposit_amount" value="<?php echo $depositArray[0]->getAmount()?>">
        
        <div class="clear"></div>

        <div class="three-btn-container">
            <input onclick="this.form.submited=this.value;"  type="submit" name="REJECT" value="REJECT" class="refund-btn-a white-button three-btn-a">
            <input onclick="this.form.submited=this.value;"  type="submit" name="ACCEPTED" value="ACCEPTED" class="shipout-btn-a black-button three-btn-a" >
        </div>

    </form>

</div>

<!-- <?php //require_once dirname(__FILE__) . '/footer.php'; ?>
<?php //include 'jsAdmin.php'; ?> -->

<script>
function goBack() {
  window.history.back();
}
</script>

<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType)
        {
            case 'ACCEPTED':
                form=document.getElementById('depositVerifiedForm');
                form.action='utilities/depositVerificationAcceptedFunction.php';
                form.submit();
            break;
            case 'REJECT':
                form=document.getElementById('depositVerifiedForm');
                form.action='utilities/depositVerificationRejectedFunction.php';
                form.submit();
            break;
        }
    }
</script>

</body>
</html>