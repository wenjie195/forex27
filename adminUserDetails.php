<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Deposit.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminUserDetails.php" />
    <meta property="og:title" content="User Details | De Xin Guo Ji 德鑫国际" />
    <title>User Details | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminUserDetails.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<div class="dark-bg overflow same-padding">
<?php include 'headerAdmin.php'; ?>
<?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
        <h1 class="menu-distance h1-title white-text text-center"><?php echo _AUD_CUSDETAILA ?></h1>
        <div class="width100 overflow blue-opa-bg padding-box radius-box">
        <table class="table-width data-table message-table">
                    <thead>
                        <tr>
                            <th><?php echo _JS_USERNAME ?></th>
                            <!-- <th>Account No.</th>
                            <th>Amount</th>
                            <th>Top-Up</th>
                            <th>Account Status</th> -->
                            <th><?php echo _AUD_ACCOUNTNO ?></th>
                            <th><?php echo _AUD_AMOUNT ?></th>
                            <th><?php echo _AUD_TOPUP ?></th>
                            <th><?php echo _AUD_ACCSTATUS ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $conn = connDB();
                        $userRows = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['useruid_details']),"s");
                        if($userRows != null)
                        {   
                            for($cnt = 0;$cnt < count($userRows) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo $userRows[$cnt]->getUsername();?></td>
                                <td><?php echo $userRows[$cnt]->getBankAccountNo();?></td>
                                <td><?php echo $userRows[$cnt]->getCredit();?></td>
                                
                                <td>
                                    <form action="adminUserAddCredit.php" method="POST">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="user_uid" value="<?php echo $userRows[$cnt]->getUid();?>">
                                            <?php echo _AUD_CLICKTOPUP ?>
                                        </button>
                                    </form>
                                </td>

                                <?php
                                $userLoginType = $userRows[$cnt]->getLoginType();
                                if($userLoginType == 1)
                                {
                                ?>
                                <td>
                                    <form action="utilities/deactivatedFunction.php" method="POST">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="useruid_details" value="<?php echo $userRows[$cnt]->getUid();?>">
                                            <?php echo _AUD_INACTIVE ?>
                                        </button>
                                    </form>
                                </td>
                                <?php
                                }
                                else
                                {
                                ?>
                                <td>
                                    <form action="utilities/activatedFunction.php" method="POST">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="useruid_details" value="<?php echo $userRows[$cnt]->getUid();?>">
                                            <?php echo _AUD_ACTIVE ?>
                                        </button>
                                    </form>
                                </td>
                                <?php
                                }
                                ?>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
        </div>

    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>