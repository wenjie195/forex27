<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/BuySell.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $depositDetails = getDeposit($conn," WHERE status = 'PENDING' ");

$tradeDetails = getBetstatus($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <?php //include 'meta.php'; ?> -->
    <meta property="og:url" content="https://dxforextrade88.com/adminEditResult.php" />
    <meta property="og:title" content="Result | De Xin Guo Ji 德鑫国际" />
    <title>Result | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminEditResult.php" />
	<!-- <?php //include 'css.php'; ?> -->
</head>
<body class="body">

<?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!-- <?php //include 'adminSidebar.php'; ?> -->

<div class="next-to-sidebar">

    <form method="POST" action="utilities/adminEditResultFunction.php" enctype="multipart/form-data">
	<h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
        	Trade ID : <?php echo $_POST['trade_uid']; ?>
        </a>
    </h1>
        <?php
            if(isset($_POST['trade_uid']))
            {
                $conn = connDB();
                $betDetails = getBetstatus($conn,"WHERE trade_uid = ? ", array("trade_uid") ,array($_POST['trade_uid']),"s");
            ?>

                <tr>
                    <td class="profile-td1">Trade ID</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $betDetails[0]->getId();?></td>
                </tr>

                <div class="clear"></div> 

                <tr>
                    <td class="profile-td1">Amount</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $betDetails[0]->getAmount();?></td>
                </tr>

                <div class="clear"></div> 

                <tr>
                    <td class="profile-td1">Timeline</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $betDetails[0]->getTimeline();?></td>
                </tr>

                <div class="clear"></div> 

                <tr>
                    <td class="profile-td1">Result (User Side)</td>
                    <td class="profile-td2">:</td>
                    <td class="profile-td3"><?php echo $betDetails[0]->getResult();?></td>
                </tr>
        
                <div class="clear"></div> 

                <!-- <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $betDetails[0]->getUid()?>">
                <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $betDetails[0]->getId()?>">
                <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $betDetails[0]->getAmount()?>">
                <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $betDetails[0]->getResult()?>"> -->

                <input type="text" id="trading_uid" name="trading_uid" value="<?php echo $betDetails[0]->getTradeUid()?>">
                <input type="text" id="trading_userid" name="trading_userid" value="<?php echo $betDetails[0]->getUid()?>">
                <input type="text" id="trading_id" name="trading_id" value="<?php echo $betDetails[0]->getId()?>">
                <input type="text" id="trading_amount" name="trading_amount" value="<?php echo $betDetails[0]->getAmount()?>">
                <input type="text" id="trading_result" name="trading_result" value="<?php echo $betDetails[0]->getResult()?>">

                <div class="clear"></div> 

            <?php
            }
        ?>

            <button class="clean red-btn margin-top30 fix300-btn" type="submit" id ="editSubmit" name ="editSubmit" ><b>Edit</b></a></button>
    </form>
</div>

</body>
</html>