<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

function submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus)
{
     if(insertDynamicData($conn,"message",array("uid","message_uid","receive_message","user_status","admin_status"),
     array($uid,$message_uid,$receiveSMS,$userStatus,$adminStatus),"sssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $message_uid = rewrite($_POST["message_uid"]);
    $receiveSMS = rewrite($_POST["message_details"]);
    $adminStatus = "GET";
    $userStatus = "SENT";

    // //for debugging
    // echo "<br>";
    // echo $message_uid."<br>";
    // echo $receiveSMS."<br>";
    // echo $adminStatus."<br>";
    // echo $userStatus."<br>";

    if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus))
    {
        header('Location: ../viewMessage.php?type=1');
    }
    else
    {
        header('Location: ../viewMessage.php?type=2');
    }

    // if(isset($_POST['message_uid']))
    // {   
    //     $tableName = array();
    //     $tableValue =  array();
    //     $stringType =  "";
    //     //echo "save to database";
    //     if($message_details)
    //     {
    //         array_push($tableName,"reply_one");
    //         array_push($tableValue,$message_details);
    //         $stringType .=  "s";
    //     }     
    //     if($adminStatus)
    //     {
    //         array_push($tableName,"admin_status");
    //         array_push($tableValue,$adminStatus);
    //         $stringType .=  "s";
    //     } 
    //     if($userStatus)
    //     {
    //         array_push($tableName,"user_status");
    //         array_push($tableValue,$userStatus);
    //         $stringType .=  "s";
    //     } 

    //     array_push($tableValue,$message_uid);
    //     $stringType .=  "s";
    //     $messageUpdated = updateDynamicData($conn,"message"," WHERE message_uid = ? ",$tableName,$tableValue,$stringType);
        
    //     if($messageUpdated)
    //     {
    //         header('Location: ../viewMessage.php?type=1');
    //     }
    //     else
    //     {
    //         header('Location: ../viewMessage.php?type=2');
    //     }
    // }
    // else
    // {
    //     header('Location: ../viewMessage.php?type=3');
    // }

}
else 
{
    // echo "gg";
    header('Location: ../index.php');
}

?>