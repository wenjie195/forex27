<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Message.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitMessage($conn,$uid,$message_uid,$receiveSMS,$adminStatus)
{
     if(insertDynamicData($conn,"message",array("uid","message_uid","receive_message","admin_status"),
     array($uid,$message_uid,$receiveSMS,$adminStatus),"ssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();
    $uid = rewrite($_POST["usertopup_id"]);
    $message_uid = md5(uniqid());
    $receiveSMS = '(系统讯息) 要求充值，请联系我!';
    // $adminStatus = "GET";
    $adminStatus = "TOPUP";

    //   FOR DEBUGGING
    // echo "<br>";
    // echo $uid."<br>";
    // echo $loginType."<br>";

    if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$adminStatus))
    {
        // $_SESSION['messageType'] = 1;
        header('Location: ../profile.php?type=1');
    }
    else
    {
        header('Location: ../profile.php?type=2');
    }

}
else
{
     header('Location: ../index.php');
}
?>
