<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $message_uid = rewrite($_POST["message_uid"]);
    $message_details = rewrite($_POST["message_details"]);
    $adminStatus = "REPLY";
    $userStatus = "GET";

    //for debugging
    // echo "<br>";
    // echo $message_uid."<br>";
    // echo $message_details."<br>";

    if(isset($_POST['message_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($message_details)
        {
            array_push($tableName,"reply_message");
            array_push($tableValue,$message_details);
            $stringType .=  "s";
        }     
        if($adminStatus)
        {
            array_push($tableName,"admin_status");
            array_push($tableValue,$adminStatus);
            $stringType .=  "s";
        } 
        if($userStatus)
        {
            array_push($tableName,"user_status");
            array_push($tableValue,$userStatus);
            $stringType .=  "s";
        } 

        array_push($tableValue,$message_uid);
        $stringType .=  "s";
        $messageUpdated = updateDynamicData($conn,"message"," WHERE message_uid = ? ",$tableName,$tableValue,$stringType);
        
        if($messageUpdated)
        {
            // echo "success";
            echo "<script>alert('successfully reply message!');window.location='../adminViewMessage.php'</script>"; 
        }
        else
        {
            // echo "fail";
            echo "<script>alert('fail reply message!');window.location='../adminViewMessage.php'</script>"; 
        }
    }
    else
    {
        // echo "dunno";
        echo "<script>alert('error!');window.location='../adminViewMessage.php'</script>"; 
    }

}
else 
{
    // echo "gg";
    header('Location: ../index.php');
}

?>