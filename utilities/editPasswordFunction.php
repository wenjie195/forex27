<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $username  = $_SESSION['username'];
     $uid  = $_SESSION['uid'];

     $editPassword_current  = $_POST['editPassword_current'];
     $editPassword_new  = $_POST['editPassword_new'];
     $editPassword_reenter  = $_POST['editPassword_reenter'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $userDetails = $user[0];
     // echo $userDetails->getUsername();

     $dbPass =  $userDetails->getPassword();
     $dbSalt =  $userDetails->getSalt();

     $editPassword_current_hashed = hash('sha256',$editPassword_current);
     $editPassword_current_hashed_salted = hash('sha256', $dbSalt . $editPassword_current_hashed);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $editPassword_current."<br>";
     // echo $editPassword_new."<br>";
     // echo $editPassword_reenter."<br>";

     if($editPassword_current_hashed_salted == $dbPass)
     {
          if(strlen($editPassword_new) >= 6 && strlen($editPassword_reenter) >= 6 )
          {
               if($editPassword_new == $editPassword_reenter)
               {
                    $password = hash('sha256',$editPassword_new);
                    $salt = substr(sha1(mt_rand()), 0, 100);
                    $finalPassword = hash('sha256', $salt.$password);

                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$uid),"sss");
                    if($passwordUpdated)
                    {
                         // $_SESSION['messageType'] = 1;
                         // header( "Location: ../editPassword.php?type=5" );
                         // echo "Update Password success ";
                         echo "<script>alert('Update Password success !');window.location='../editPassword.php'</script>";
                    }
                    else 
                    {
                         //echo "//server problem ";
                         echo "<script>alert('Fail to update password !');window.location='../editPassword.php'</script>";
                    }
               }
               else 
               {
                    // echo "password must be same with reenter ";
                    echo "<script>alert('password must be same with reenter ');window.location='../editPassword.php'</script>";
               }
          }
          else 
          {
               // echo "password length must be more than 6 ";
               echo "<script>alert('password length must be more than 6');window.location='../editPassword.php'</script>";
          }
     }
     else 
     {
          // echo "current password is not the same as previous ";
          echo "<script>alert('current password is not the same as previous ');window.location='../editPassword.php'</script>";
     }    
}
?>