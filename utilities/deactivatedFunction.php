<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["useruid_details"]);
    $loginType = "2";

    //   FOR DEBUGGING
    // echo "<br>";
    // echo $uid."<br>";
    // echo $loginType."<br>";

    if(isset($_POST['useruid_details']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($loginType)
        {
            array_push($tableName,"login_type");
            array_push($tableValue,$loginType);
            $stringType .=  "i";
        }    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $loginTypeUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        
        if($loginTypeUpdated)
        {
            // echo "success";
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminMemberList.php?success');
            // $_SESSION['messageType'] = 1;
            header('Location: ../adminMemberList.php?type=2');
        }
        else
        {
            // header('Location: ../adminMemberList.php?fail');
            // $_SESSION['messageType'] = 1;
            header('Location: ../adminMemberList.php?type=3');
        }
    }
    else
    {
        // header('Location: ../adminMemberList.php?dunno');
        // $_SESSION['messageType'] = 1;
        header('Location: ../adminMemberList.php?type=4');
    }

}
else
{
     header('Location: ../index.php');
}
?>
