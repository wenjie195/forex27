<?php

// Set Language variable
if(isset($_GET['lang']) && !empty($_GET['lang'])){
    $_SESSION['lang'] = $_GET['lang'];

    if(isset($_SESSION['lang']) && $_SESSION['lang'] != $_GET['lang']){
        echo "<script type='text/javascript'> location.reload(); </script>";
    }
}

// Include Language file
if(isset($_SESSION['lang'])){
    require_once dirname(__FILE__) . "/../lang/".$_SESSION['lang'].".php";
}else{
    require_once dirname(__FILE__) . "/../lang/ch.php";
    // require_once dirname(__FILE__) . "/../lang/en.php";
}

function getJsErrorValidationMsgArray(){
    return '
        <script>
            let errorMsgArrayTranslationForJsValidation = {
                _MAINJS_ATTENTION: "'._MAINJS_ATTENTION.'",
                _MAINJS_ENTER_BELOW_INFO: "'._MAINJS_ENTER_BELOW_INFO.'",
                _MAINJS_ENTER_USERNAME: "'._MAINJS_ENTER_USERNAME.'",
                _MAINJS_ENTER_EMAIL: "'._MAINJS_ENTER_EMAIL.'",
                _MAINJS_ENTER_ICNO: "'._MAINJS_ENTER_ICNO.'",
                _MAINJS_SELECT_COUNTRY: "'._MAINJS_SELECT_COUNTRY.'",
                _MAINJS_ENTER_PHONENO: "'._MAINJS_ENTER_PHONENO.'",
                _MAINJS_ENTER_PASSWORD: "'._MAINJS_ENTER_PASSWORD.'",
                _MAINJS_ENTER_CONFIRM_PASSWORD: "'._MAINJS_ENTER_CONFIRM_PASSWORD.'",
                _MAINJS_ACCEPT_TERMS: "'._MAINJS_ACCEPT_TERMS.'",
                _MAINJS_VALID_EMAIL_ADDRESS: "'._MAINJS_VALID_EMAIL_ADDRESS.'",
                _MAINJS_ICNO_WRONG_: "'._MAINJS_ICNO_WRONG_.'",
                _MAINJS_ENTER_CURRENT_PASSWORD: "'._MAINJS_ENTER_CURRENT_PASSWORD.'",
                _MAINJS_ENTER_NEW_PASSWORD: "'._MAINJS_ENTER_NEW_PASSWORD.'",
                _MAINJS_VALID_PASSWORD_LENGTH: "'._MAINJS_VALID_PASSWORD_LENGTH.'",
                _MAINJS_REENTER_NEW_PASSWORD: "'._MAINJS_REENTER_NEW_PASSWORD.'"
            };
        
        </script>
    ';
}