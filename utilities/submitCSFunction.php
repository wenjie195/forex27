<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus)
{
     if(insertDynamicData($conn,"message",array("uid","message_uid","receive_message","user_status","admin_status"),
     array($uid,$message_uid,$receiveSMS,$userStatus,$adminStatus),"sssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $message_uid = md5(uniqid());
     $uid = rewrite($_POST["sender_uid"]);
     $receiveSMS = rewrite($_POST["message_details"]);
     $userStatus = "SENT";
     $adminStatus = "GET";

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $message_uid."<br>";
     // echo $uid."<br>";
     // echo $receiveSMS."<br>";


     if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus))
     {
          header('Location: ../viewMessage.php?type=1');
     }
     else
     {
          header('Location: ../viewMessage.php?type=2');
     }

}
else
{
     header('Location: ../viewMessage.php?type=3');
}
?>
