<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/BuySell.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$page = $_SERVER['PHP_SELF'];
$sec = "2";

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

//30sec
$tradeDetailsA = getBetstatus($conn," WHERE timeline = '30' AND result_edited = 'DRAW' AND status != 'ORI' ORDER BY amount DESC");
$tradeDetailsAWin = getBetstatus($conn," WHERE timeline = '30' AND result_edited = 'LOSE' AND status != 'ORI' ORDER BY amount DESC");
$tradeDetailsALose = getBetstatus($conn," WHERE timeline = '30' AND result_edited = 'WIN' AND status != 'ORI' ORDER BY amount DESC");
//60sec
$tradeDetailsB = getBetstatus($conn," WHERE timeline = '60' AND result_edited = 'DRAW' AND status != 'ORI' ORDER BY amount DESC");
$tradeDetailsBWin = getBetstatus($conn," WHERE timeline = '60' AND result_edited = 'LOSE' AND status != 'ORI' ORDER BY amount DESC");
$tradeDetailsBLose = getBetstatus($conn," WHERE timeline = '60' AND result_edited = 'WIN' AND status != 'ORI' ORDER BY amount DESC");
//180sec
$tradeDetailsC = getBetstatus($conn," WHERE timeline = '180' AND result_edited = 'DRAW' AND status != 'ORI' ORDER BY amount DESC");
$tradeDetailsCWin = getBetstatus($conn," WHERE timeline = '180' AND result_edited = 'LOSE' AND status != 'ORI' ORDER BY amount DESC");
$tradeDetailsCLose = getBetstatus($conn," WHERE timeline = '180' AND result_edited = 'WIN' AND status != 'ORI' ORDER BY amount DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminEditedCurrentTrade.php" />
    <meta property="og:title" content="Current Trade | De Xin Guo Ji 德鑫国际" />
    <title>Current Trade | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminEditedCurrentTrade.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
    <h1 class="menu-distance h1-title white-text text-center"><a href="adminCurrentTrade.php"><?php echo _SIDEBAR_CURRENT_TRADE ?></a> | <span class="blue-link"><?php echo _ADMINCURRENTTRADE_EDITEDCT ?></span> </h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box">
        <div class="three-div-width">
        	<div class="fake-header-div">
            	30 <?php echo _USERDASHBOARD_SEC ?>
            </div>
            <table class="width100 data-table trade-table">
            	<thead>
                	<tr>
                    	<th><?php echo _ADMINCURRENTTRADE_UNREALIZED_GAIN ?></th>
                        <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_LOSS ?></th>
                	</tr>
                </thead>
            	<tbody>
                	<tr>
                        <td>
                        <?php
                            if($tradeDetailsAWin != null)
                            {
                                for($cnt = 0;$cnt < count($tradeDetailsAWin) ;$cnt++)
                                {
                                ?>  
                                <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                    <button class="clean green-down-btn border-0 width100-ow" type="submit" name="trade_uid" value="<?php echo $tradeDetailsAWin[$cnt]->getTradeUid();?>">
                                        <p class="buy-text"><?php echo $tradeDetailsAWin[$cnt]->getUsername();?></p>
                                        <p class="buy-text"><?php echo $tradeDetailsAWin[$cnt]->getAmount();?></p>
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetailsAWin[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetailsAWin[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetailsAWin[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetailsAWin[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetailsAWin[$cnt]->getResult()?>">
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetailsAWin[$cnt]->getResultEdited()?>">
                                    </button>
                                </form>
                                <?php
                                }
                            }
                        ?>
                        </td>
                        <td>
                        <?php
                            if($tradeDetailsALose != null)
                            {
                                for($cnt = 0;$cnt < count($tradeDetailsALose) ;$cnt++)
                                {
                                ?>
                                <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                    <button class="clean red-down-btn border-0 width100-ow" type="submit" name="trade_uid" value="<?php echo $tradeDetailsALose[$cnt]->getTradeUid();?>">
                                        <p class="buy-text"><?php echo $tradeDetailsALose[$cnt]->getUsername();?></p>
                                        <p class="buy-text"><?php echo $tradeDetailsALose[$cnt]->getAmount();?></p>
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetailsALose[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetailsALose[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetailsALose[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetailsALose[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetailsALose[$cnt]->getResult()?>">
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetailsALose[$cnt]->getResultEdited()?>">
                                    </button>
                                </form>
                                <?php
                                }
                            }
                        ?>
                        </td>
                    </tr>
                	<tr>
                        <td>
                        <?php
                            if($tradeDetailsA != null)
                            {
                                for($cnt = 0;$cnt < count($tradeDetailsA) ;$cnt++)
                                {
                                ?>
                                <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                    <button class="clean white-down-btn border-0 width100-ow" type="submit" name="trade_uid" value="<?php echo $tradeDetailsA[$cnt]->getTradeUid();?>">
                                        <p class="buy-text"><?php echo $tradeDetailsA[$cnt]->getUsername();?></p>
                                        <p class="buy-text"><?php echo $tradeDetailsA[$cnt]->getAmount();?></p>
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetailsA[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetailsA[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetailsA[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetailsA[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetailsA[$cnt]->getResult()?>">
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetailsA[$cnt]->getResultEdited()?>">
                                    </button>
                                </form>
                                <?php
                                }
                            }
                        ?>
                        </td>
                        <td></td>
                    </tr>                    
                </tbody>
            </table>
    	</div>   
        
        <div class="three-div-width">
        	<div class="fake-header-div">
            	60 <?php echo _USERDASHBOARD_SEC ?>
            </div>
            <table class="width100 data-table trade-table">
            	<thead>
                	<tr>
                    	<th><?php echo _ADMINCURRENTTRADE_UNREALIZED_GAIN ?></th>
                        <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_LOSS ?></th>
                	</tr>
                </thead>
            	<tbody>
                	<tr>
                        <td>
                        <?php
                            if($tradeDetailsBWin != null)
                            {
                                for($cnt = 0;$cnt < count($tradeDetailsBWin) ;$cnt++)
                                {
                                ?>  
                                <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                    <button class="clean green-down-btn border-0 width100-ow" type="submit" name="trade_uid" value="<?php echo $tradeDetailsBWin[$cnt]->getTradeUid();?>">
                                        <p class="buy-text"><?php echo $tradeDetailsBWin[$cnt]->getUsername();?></p>
                                        <p class="buy-text"><?php echo $tradeDetailsBWin[$cnt]->getAmount();?></p>
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetailsBWin[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetailsBWin[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetailsBWin[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetailsBWin[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetailsBWin[$cnt]->getResult()?>">
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetailsBWin[$cnt]->getResultEdited()?>">
                                    </button>
                                </form>
                                <?php
                                }
                            }
                        ?>
                        </td>
                        <td>
                        <?php
                            if($tradeDetailsBLose != null)
                            {
                                for($cnt = 0;$cnt < count($tradeDetailsBLose) ;$cnt++)
                                {
                                ?>
                                <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                    <button class="clean red-down-btn border-0 width100-ow" type="submit" name="trade_uid" value="<?php echo $tradeDetailsBLose[$cnt]->getTradeUid();?>">
                                        <p class="buy-text"><?php echo $tradeDetailsBLose[$cnt]->getUsername();?></p>
                                        <p class="buy-text"><?php echo $tradeDetailsBLose[$cnt]->getAmount();?></p>
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetailsBLose[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetailsBLose[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetailsBLose[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetailsBLose[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetailsBLose[$cnt]->getResult()?>">
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetailsBLose[$cnt]->getResultEdited()?>">
                                    </button>
                                </form>
                                <?php
                                }
                            }
                        ?>
                        </td>
                    </tr>
                	<tr>
                        <td>
                        <?php
                            if($tradeDetailsB != null)
                            {
                                for($cnt = 0;$cnt < count($tradeDetailsB) ;$cnt++)
                                {
                                ?>
                                <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                    <button class="clean white-down-btn border-0 width100-ow" type="submit" name="trade_uid" value="<?php echo $tradeDetailsB[$cnt]->getTradeUid();?>">
                                        <p class="buy-text"><?php echo $tradeDetailsB[$cnt]->getUsername();?></p>
                                        <p class="buy-text"><?php echo $tradeDetailsB[$cnt]->getAmount();?></p>
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetailsB[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetailsB[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetailsB[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetailsB[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetailsB[$cnt]->getResult()?>">
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetailsB[$cnt]->getResultEdited()?>">
                                    </button>
                                </form>
                                <?php
                                }
                            }
                        ?>
                        </td>
                        <td></td>
                    </tr>                    
                </tbody>
            </table>
    	</div>   

    	<div class="three-div-width">
        	<div class="fake-header-div">
            	180 <?php echo _USERDASHBOARD_SEC ?>
            </div>
            <table class="width100 data-table trade-table">
            <!-- <form action="utilities/adminEditResultFunction.php" method="POST"> -->
            	<thead>
                	<tr>
                    	<th><?php echo _ADMINCURRENTTRADE_UNREALIZED_GAIN ?></th>
                        <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_LOSS ?></th>
                	</tr>
                </thead>
            	<tbody>
                	<tr>
                        <td>
                        <?php
                            if($tradeDetailsCWin != null)
                            {
                                for($cnt = 0;$cnt < count($tradeDetailsCWin) ;$cnt++)
                                {
                                ?>  
                                <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                    <button class="clean green-down-btn border-0 width100-ow" type="submit" name="trade_uid" value="<?php echo $tradeDetailsCWin[$cnt]->getTradeUid();?>">
                                        <p class="buy-text"><?php echo $tradeDetailsCWin[$cnt]->getUsername();?></p>
                                        <p class="buy-text"><?php echo $tradeDetailsCWin[$cnt]->getAmount();?></p>
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetailsCWin[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetailsCWin[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetailsCWin[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetailsCWin[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetailsCWin[$cnt]->getResult()?>">
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetailsCWin[$cnt]->getResultEdited()?>">
                                    </button>
                                </form>   
                                <?php
                                }
                            }
                        ?>
                        </td>
                        <td>
                        <?php
                            if($tradeDetailsCLose != null)
                            {
                                for($cnt = 0;$cnt < count($tradeDetailsCLose) ;$cnt++)
                                {
                                ?>
                                <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                    <button class="clean red-down-btn border-0 width100-ow" type="submit" name="trade_uid" value="<?php echo $tradeDetailsCLose[$cnt]->getTradeUid();?>">
                                        <p class="buy-text"><?php echo $tradeDetailsCLose[$cnt]->getUsername();?></p>
                                        <p class="buy-text"><?php echo $tradeDetailsCLose[$cnt]->getAmount();?></p>
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetailsCLose[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetailsCLose[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetailsCLose[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetailsCLose[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetailsCLose[$cnt]->getResult()?>">         
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetailsCLose[$cnt]->getResultEdited()?>">                           
                                    </button>
                                </form>   
                                <?php
                                }
                            }
                        ?>
                        </td>
                    </tr>
                	<tr>
                        <td>
                        <!-- Draw result not yet solve -->
                        <?php
                            if($tradeDetailsC != null)
                            {
                                for($cnt = 0;$cnt < count($tradeDetailsC) ;$cnt++)
                                {
                                ?>
                                <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                                    <button class="clean white-down-btn border-0 width100-ow" type="submit" name="trade_uid" value="<?php echo $tradeDetailsC[$cnt]->getTradeUid();?>">
                                        <p class="buy-text"><?php echo $tradeDetailsC[$cnt]->getUsername();?></p>
                                        <p class="buy-text"><?php echo $tradeDetailsC[$cnt]->getAmount();?></p>
                                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetailsC[$cnt]->getTradeUid()?>">
                                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetailsC[$cnt]->getUid()?>">
                                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetailsC[$cnt]->getId()?>">
                                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetailsC[$cnt]->getAmount()?>">
                                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetailsC[$cnt]->getResult()?>">        
                                        <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetailsC[$cnt]->getResultEdited()?>">                            
                                    </button>
                                </form>    
                                <?php
                                }
                            }
                        ?>
                        </td>
                        <td></td>
                    </tr>                    
                </tbody>
            <!-- </form>     -->
            </table>
    	</div>        
         
    </div>
    </div>
</div>
<?php include 'js.php'; ?>
</body>
</html>