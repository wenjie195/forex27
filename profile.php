<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/BetStatus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$messageValue = getMessage($conn," WHERE uid = ? AND reply_message != '' ",array("uid"),array($uid),"s");

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i:s');
$playTime = $dt->format('s');


function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/profile.php" />
    <meta property="og:title" content="Profile | De Xin Guo Ji 德鑫国际" />
    <title>Profile | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/profile.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
    <?php include 'headerAfterLogin.php'; ?>
    <!-- <?php //include 'customerService.php'; ?> -->
    <div class="overflow small-web-width">
    	<?php include 'userDetails.php'; ?>
    	<a href="editProfile.php">
            <div class="width100 line-menu hover1 hover-a small-distance profile-menu-distance">
                <div class="left-img-icon">
                	<img src="img/profile-1.png" alt="<?php echo _PROFILE_PERSONAL_DETAILS ?>" title="<?php echo _PROFILE_PERSONAL_DETAILS ?>" class="hover1a profile-icon">
                    <img src="img/profile-2.png" alt="<?php echo _PROFILE_PERSONAL_DETAILS ?>" title="<?php echo _PROFILE_PERSONAL_DETAILS ?>" class="hover1b profile-icon">
                </div>
                <div class="mid-profile-menu white-to-blue">
                	<?php echo _PROFILE_PERSONAL_DETAILS ?>
                </div>
                <div class="right-profile-arrow">
                	<img src="img/arrow.png" class="hover1a arrow-icon">
                    <img src="img/arrow2.png" class="hover1b arrow-icon">                	
                </div>
            </div>
        </a>
    	<a href="topupHistory.php">
            <div class="width100 line-menu hover1 hover-a">
                <div class="left-img-icon">
                	<img src="img/top-up1.png" alt="<?php echo _PROFILE_TOPUP_HISTORY ?>" title="<?php echo _PROFILE_TOPUP_HISTORY ?>" class="hover1a profile-icon">
                    <img src="img/top-up2.png" alt="<?php echo _PROFILE_TOPUP_HISTORY ?>" title="<?php echo _PROFILE_TOPUP_HISTORY ?>" class="hover1b profile-icon">
                </div>
                <div class="mid-profile-menu white-to-blue">
                	<?php echo _PROFILE_TOPUP_HISTORY ?>
                </div>
                <div class="right-profile-arrow">
                	<img src="img/arrow.png" class="hover1a arrow-icon">
                    <img src="img/arrow2.png" class="hover1b arrow-icon">                	
                </div>
            </div>
        </a>

    	<a href="withdrawHistory.php">
            <div class="width100 line-menu hover1 hover-a">
                <div class="left-img-icon">
                	<img src="img/withdraw1.png" alt="<?php echo _PROFILE_WITHDRAW_HISTORY ?>" title="<?php echo _PROFILE_WITHDRAW_HISTORY ?>" class="hover1a profile-icon">
                    <img src="img/withdraw-2.png" alt="<?php echo _PROFILE_WITHDRAW_HISTORY ?>" title="<?php echo _PROFILE_WITHDRAW_HISTORY ?>" class="hover1b profile-icon">
                </div>
                <div class="mid-profile-menu white-to-blue">
                	<?php echo _PROFILE_WITHDRAW_HISTORY ?>
                </div>
                <div class="right-profile-arrow">
                	<img src="img/arrow.png" class="hover1a arrow-icon">
                    <img src="img/arrow2.png" class="hover1b arrow-icon">                	
                </div>
            </div>
        </a>         
    	<a href="viewMessage.php">
            <div class="width100 line-menu hover1 hover-a last-line-menu">
                <div class="left-img-icon">
                	<img src="img/view-message.png" alt="<?php echo _USERDASHBOARD_VIEW_MESSAGE ?>" title="<?php echo _USERDASHBOARD_VIEW_MESSAGE ?>" class="hover1a profile-icon">
                    <img src="img/view-message2.png" alt="<?php echo _USERDASHBOARD_VIEW_MESSAGE ?>" title="<?php echo _USERDASHBOARD_VIEW_MESSAGE ?>" class="hover1b profile-icon">
                </div>
                <div class="mid-profile-menu white-to-blue">
                	<?php echo _USERDASHBOARD_VIEW_MESSAGE ?>
                </div>
                <div class="right-profile-arrow">
                	<img src="img/arrow.png" class="hover1a arrow-icon">
                    <img src="img/arrow2.png" class="hover1b arrow-icon">                	
                </div>
            </div>
        </a>                
	</div>
</div>
      <div id="withdraw-modal" class="modal-css">
          <!-- Modal content -->
          <div class="modal-content-css forgot-modal-content login-modal-content signup-modal-content ">
              <span class="close-css close-withdraw">&times;</span>
              <h1 class="h1-title white-text text-center"><?php echo _USERDASHBOARD_WITHDRAW ?></h1>
              <!-- <form> -->
              <form action="utilities/submitWithdrawalFunction.php" method="POST">
              <div class="up-bottom-border">
                  <!-- <p class="input-top-text"><?php //echo _USERDASHBOARD_BALANCE ?></p> -->
                  <!-- <p class="clean de-input no-input-style">$1,000,000</p> -->
                  <!-- <p class="clean de-input no-input-style">$<?php //echo $credit;?></p> -->
                  <p class="input-top-text"><?php echo _USERDASHBOARD_BN ?></p>
                  <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_BN ?>" id="withdrawal_bank_name" name="withdrawal_bank_name" required>
                  <p class="input-top-text"><?php echo _USERDASHBOARD_BENEFICIALNAME ?></p>
                  <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_BENEFICIALNAME ?>" id="withdrawal_bank_acc_holder" name="withdrawal_bank_acc_holder" required>
                  <p class="input-top-text"><?php echo _USERDASHBOARD_BAN ?></p>
                  <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_BAN ?>" id="withdrawal_bank_acc_number" name="withdrawal_bank_acc_number" required>
                  <p class="input-top-text"><?php echo _JS_WITHDRAW_AMOUNT ?></p>
                  <!-- <input class="clean de-input" type="number" placeholder="<?php echo _USERDASHBOARD_AMOUNT ?>" required> -->
                  <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_AMOUNT ?>"  id="withdrawal_amount" name="withdrawal_amount" required>
                    <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
                    <select class="clean  de-input" id="trade_type" name="trade_type" required>
                        <option value="Buy" name="Buy">Country 1</option>
                        <option value="Sell" name="Sell">Country 2</option>
                    </select>                  
                  
              </div>

              <input type="hidden" id="withdrawal_uid" name="withdrawal_uid" value="<?php echo $userDetails->getUid();?>" readonly>
              <input type="hidden" id="withdrawal_currentcredit" name="withdrawal_currentcredit" value="<?php echo $userDetails->getCredit();?>" readonly>

              <div class="clear"></div>
                  <button class="clean blue-button width100 small-distance small-distance-bottom"><?php echo _USERDASHBOARD_WITHDRAW ?></button>
              <div class="clear"></div>
              </form>
          </div>
      </div>

      <!-- withdraw Modal -->
      <!-- <div id="customerservice-modal" class="modal-css">
          <div class="modal-content-css forgot-modal-content login-modal-content">
              <span class="close-css close-customerservice">&times;</span>
              <h1 class="h1-title white-text text-center">Customer Service</h1>
              <form action="utilities/submitCSFunction.php" method="POST">
              <div class="up-bottom-border">
                  <p class="input-top-text">Message</p>
                  <input class="clean de-input" type="text" placeholder="Your Message" id="message_details" name="message_details" required>
              </div>
              <input type="hidden" id="sender_uid" name="sender_uid" value="<?php echo $userDetails->getUid();?>" readonly>
              <div class="clear"></div>
                  <button class="clean blue-button width100 small-distance small-distance-bottom">Click To Sent</button>
              <div class="clear"></div>
              </form>
          </div>
      </div> -->

        <!-- withdraw Modal -->
        <div id="customerservice-modal" class="modal-css">
            <!-- Modal content -->
            <div class="modal-content-css forgot-modal-content login-modal-content">
                <span class="close-css close-customerservice">&times;</span>
                <h1 class="h1-title white-text text-center"><?php echo _USERDASHBOARD_CUSTOMER_SERVICE ?></h1>
                <form action="utilities/submitCSFunction.php" method="POST">
                    <div class="up-bottom-border">
                        <p class="input-top-text"><?php echo _HEADER_MESSAGE ?></p>
                        <input class="clean de-input" type="text" placeholder="<?php echo _HEADER_MESSAGE ?>" id="message_details" name="message_details" required>
                    </div>
                    <input type="hidden" id="sender_uid" name="sender_uid" value="<?php echo $userDetails->getUid();?>" readonly>
                    <div class="clear"></div>
                    <button class="clean blue-button width100 small-distance small-distance-bottom"><?php echo _USERDASHBOARD_SUBMIT ?></button>
                    <div class="clear"></div>
                </form>
            </div>
        </div>

<?php include 'js.php'; ?>

</body>


</html>
