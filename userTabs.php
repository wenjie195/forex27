<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
// set API Endpoint and API key
// $endpoint = 'latest';
// $access_key = 'ee34cf5a0d55806abbd902c8745af340';

// Initialize CURL:
// $ch = curl_init('http://data.fixer.io/api/'.$endpoint.'?access_key='.$access_key.'');
// $ch = curl_init('https://api.polygon.io/v1/last_quote/currencies/AUD/EUR?apiKey=CIS_tP7aDeaI0NQZTKnebo4J_NH7ld57R1ZMs4');
// $ch = curl_init('https://financialmodelingprep.com/api/v3/forex');
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
// // Store the data:
// $json = curl_exec($ch);
// curl_close($ch);
//
// // Decode JSON response:
// $exchangeRates = json_decode($json, true);
$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"x-rapidapi-host: currency-exchange.p.rapidapi.com",
		"x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$exchangeRates = json_decode($response, true);
}

// print_r($exchangeRates['forexList']);
// Access the exchange rate values, e.g. GBP:

?>
		<table class="table-width data-table">
        	<thead>
            	<tr>
                	<!-- <th>
                    	<form>
                    		<input class="white-border-input search-font" placeholder="<?php //echo _USERDASHBOARD_SEARCH ?>" type="text">
                        	<button class="clean search-btn hover1">
								<img src="img/search.png" class="search-img hover1a" alt="<?php //echo _USERDASHBOARD_SEARCH ?>" title="<?php //echo _USERDASHBOARD_SEARCH ?>">
								<img src="img/search2.png" class="search-img hover1b" alt="<?php //echo _USERDASHBOARD_SEARCH ?>" title="<?php //echo _USERDASHBOARD_SEARCH ?>">
							</button>
                        </form>
					</th> -->
					<th><?php echo _USERDASHBOARD_PRODUCT ?></th>
					<th></th>
                    <th><?php echo _USERDASHBOARD_BUY ?></th>
                    <th><?php echo _USERDASHBOARD_SELL ?></th>
                    <th><?php echo _USERDASHBOARD_CHANGE ?></th>
                    
                </tr>
            </thead>
            <tbody>
				<?php
				if ($exchangeRates)
				{

							// $currencyArray = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,USD/CNY,USD/HKD,EUR/GBP,USD/KRW,AUD/JPY,NZD/USD");
							//$currencyArray = ("欧元/美元,美元/加拿大元,英镑/日元,澳大利亚元/美元,美元/日元,英镑/美元,美元/瑞士法郎,欧元/澳元,欧元/日元,美元/中国人民币,美元/港币,欧元/英镑,美元/韩元,澳大利亚元/日元,新西兰元/美元");

							// $imageArray = ('<img src="img/analysis2.png" alt="">,<img src="img/customer-service.png" alt="">,
							// 				<img src="img/trade2.png" alt="">,<img src="img/withdraw2.png" alt="">,<img src="img/analysis2.png" alt="">,
							// 				<img src="img/customer-service.png" alt="">,<img src="img/trade2.png" alt="">');

							if(isset($_SESSION['lang']) && $_SESSION['lang'] != isset($_GET['lang']))
							{
								$currencyArray = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,EUR/GBP,AUD/JPY,NZD/USD,EUR/CHF,GBP/CAD,CAD/JPY");
							}
							else
							{
								$currencyArray = ("欧元/美元,美元/加拿大元,英镑/日元,澳大利亚元/美元,美元/日元,英镑/美元,美元/瑞士法郎,欧元/澳元,欧元/日元,欧元/英镑,澳大利亚元/日元,新西兰元/美元,欧元/瑞士法郎,英镑/加拿大元,加拿大元/日元");
							}

							// $imageArray = ('<div class="coin-div eu-div">E/U</div>,
							// 				<div class="coin-div uc-div">U/C</div>,
							// 				<div class="coin-div gj-div">G/J</div>,
							// 				<div class="coin-div au-div">A/U</div>,
							// 				<div class="coin-div uj-div">U/J</div>,
							// 				<div class="coin-div gu-div">G/U</div>,
							// 				<div class="coin-div uc-div">U/C</div>,
							// 				<div class="coin-div ea-div">E/A</div>,
							// 				<div class="coin-div ej-div">E/J</div>,
							// 				<div class="coin-div uc-div">U/C</div>,
							// 				<div class="coin-div uh-div">U/H</div>,
							// 				<div class="coin-div eg-div">E/G</div>,
							// 				<div class="coin-dikv uc-div">U/K</div>,
							// 				<div class="coin-div aj-div">A/J</div>,
							// 				<div class="coin-div nu-div">N/U</div>,');

							$imageArray = ('<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
											<img src="img/us.png" alt="US" title="US" class="country-flag">,
											<img src="img/british.png" alt="British" title="British" class="country-flag">,
											<img src="img/australia.png" alt="Australia" title="Australia" class="country-flag">,
											<img src="img/us.png" alt="US" title="US" class="country-flag">,
											<img src="img/british.png" alt="British" title="British" class="country-flag">,
											<img src="img/us.png" alt="US" title="US" class="country-flag">,
											<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
											<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
											<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
											<img src="img/australia.png" alt="Australia" title="Australia" class="country-flag">,
											<img src="img/new-zealand.png" alt="Australia" title="Australia" class="country-flag">,
											<img src="img/europe.png" alt="Australia" title="Australia" class="country-flag">,
											<img src="img/british.png" alt="British" title="British" class="country-flag">,
											<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag">,');

							$imageArrayExplode = explode(",",$imageArray);
							
							$image2Array = ('<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
											<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
											<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
											<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
											<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
											<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
											<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
											<img src="img/australia.png" alt="Australia" title="Australia" class="country-flag country-flag2">,
											<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
											<img src="img/british.png" alt="British" title="British" class="country-flag country-flag2">,
											<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
											<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
											<img src="img/swiss-franc.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
											<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
											<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,');

							$image2ArrayExplode = explode(",",$image2Array);
						// $currencyArrayWithSlash = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,USD/CNY,USD/HKD,EUR/GBP,USD/KRW,AUD/JPY,NZD/USD");
						$currencyArrayWithSlash = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,EUR/GBP,AUD/JPY,NZD/USD,EUR/CHF,GBP/CAD,CAD/JPY");

					$currencyArrayExplode = explode(",",$currencyArray);
					$currencyArrayWithSlashExplode = explode(",",$currencyArrayWithSlash);
					?><input type="hidden" id="total" value="<?php echo count($currencyArrayExplode); ?>"> <?php
					for ($cntAA=0; $cntAA <count($currencyArrayExplode) ; $cntAA++)
					{
						for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
						{
							$selectCountry = str_replace("C:","",$exchangeRates['forexList'][$cnt]['ticker']);
							if ($selectCountry == $currencyArrayWithSlashExplode[$cntAA])
							{
								?>
								<tr class="open-buy">
									<td ><?php echo $imageArrayExplode[$cntAA]; ?> <?php  echo $currencyArrayExplode[$cntAA] ; ?><?php echo $image2ArrayExplode[$cntAA]; ?></td>

									<td ></td>
<td>
	<button id="<?php echo "currency_name".$cntAA ?>" value="<?php echo  $currencyArrayWithSlashExplode[$cntAA]?>" class="clean blue-button open-buy green-btn table-btn-font fix-100">
		<?php echo number_format($exchangeRates['forexList'][$cnt]['ask'], 4); ?>
	</button>
</td>
<td>
	<button id="<?php echo "currency_nameSell".$cntAA ?>" value="<?php echo  $currencyArrayWithSlashExplode[$cntAA]?>"  class="clean blue-button open-sell red-btn table-btn-font fix-100" >
		<?php   echo number_format($exchangeRates['forexList'][$cnt]['bid'], 4); ?>
	</button>
</td>

									<!-- <td><?php //echo number_format($exchangeRates['forexList'][$cnt]['bid'], 4); ?></td>
									<td><?php //echo number_format($exchangeRates['forexList'][$cnt]['ask'], 4); ?></td> -->


									<?php if ($exchangeRates['forexList'][$cnt]['changes'] < 0)
									{
									?>
										<td class="red-text"><?php   echo number_format($exchangeRates['forexList'][$cnt]['changes'], 4)."<br>"; ?>
									<?php
									}
									elseif ($exchangeRates['forexList'][$cnt]['changes'] >= 0)
									{
									?>
										<td class="green-text"><?php   echo number_format($exchangeRates['forexList'][$cnt]['changes'], 4)."<br>"; ?>
									<?php
									}
									?>

								</tr>
								<?php
							}
						}
					}
				}
				?>
            </tbody>
		</table>

				<script type="text/javascript">
				var total = $("#total").val();
				for (var i = 0; i < total; i++)
				{
					$('#currency_name'+i+'').click( function()
					{
						var currenyName = $(this).val();
						$("#buy-modal").fadeIn(function(){
							$("#buy-modal").show();
							$("#trade_type").empty();
							$("#trade_type").append("<option>BUY</option>")
						} );
						$("#getCurrenyName").html("<a>"+currenyName+"</a>");
						$("#getCurrency").append("<input type='hidden' name='trade_currency' value="+currenyName+">");
					});

					$('#currency_nameSell'+i+'').click( function()
					{
						var currenyName = $(this).val();
						$("#buy-modal").fadeIn(function(){
							$("#buy-modal").show();
							$("#trade_type").empty();
							$("#trade_type").append("<option>SELL</option>")
						} );
						$("#getCurrenyName").html("<a>"+currenyName+"</a>");
						$("#getCurrency").append("<input type='hidden' name='trade_currency' value="+currenyName+">");
					});
				}
				</script>
