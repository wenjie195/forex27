<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/accountCreation.php" />
    <meta property="og:title" content="Account Creation | De Xin Guo Ji 德鑫国际" />
    <title>Account Creation | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/accountCreation.php" />

    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
	<?php include 'sidebar.php'; ?>
	<div class="same-padding2 menu-distance">
    <h1 class="menu-distance h1-title white-text text-center"><?php echo _SIDEBAR_ACC_CREATION ?></h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box smaller-box">
        
        <form action="utilities/addNewRefereeFunction.php" method="POST">

        <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
        <input class="clean de-input p6"  type="text" placeholder="<?php echo _JS_USERNAME ?>" id="register_username" name="register_username" required> 
        
        <div class="clear"></div>

        <div class="dual-input-div">
            <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
            <input class="clean de-input p6"  type="email" placeholder="<?php echo _JS_EMAIL ?>" id="register_email_user" name="register_email_user" required> 
        </div>

		<div class="dual-input-div second-dual-input">
            <p class="input-top-text"><?php echo _JS_PHONE ?></p>
            <input class="clean de-input p6"  type="text" placeholder="<?php echo _JS_PHONE ?>" id="register_phone" name="register_phone" required> 
        </div>

        <div class="clear"></div> 

        <div class="dual-input-div">
            <p class="input-top-text"><?php echo _JS_PASSWORD ?></p>
            <input class="clean de-input"  type="password" placeholder="<?php echo _JS_PASSWORD ?>" id="register_password" name="register_password" required>  
        </div>

        <div class="dual-input-div second-dual-input">
            <p class="input-top-text"><?php echo _JS_RETYPE_PASSWORD ?></p>
            <input class="clean de-input"  type="password" placeholder="<?php echo _JS_RETYPE_PASSWORD ?>" id="register_retype_password" name="register_retype_password" required> 
        </div>    

        <div class="clear"></div>      

        <div class="dual-input-div">
            <p class="input-top-text"><?php echo _ACC_CREATION_ACC_TYPE ?></p>
            <select class="clean de-input" id="register_accounttype" name="register_accounttype" required>
                <option value="1" name="1"><?php echo _ACC_CREATION_CUSTOMER ?></option>
                <option value="0" name="0"><?php echo _ACC_CREATION_ADMIN ?></option>
            </select>
        </div>

        <div class="dual-input-div second-dual-input">
            <p class="input-top-text"><?php echo _JS_RETYPE_REFERRER_NAME ?></p>
            <input class="clean de-input p6" type="text" placeholder="<?php echo _JS_RETYPE_REFERRER_NAME ?>" id="register_referral_name" name="register_referral_name">  
        </div>

        <div class="clear"></div>         
        
        <button class="clean blue-button mid-button-width small-distance small-distance-bottom"><?php echo _JS_SUBMIT ?></button>

        <div class="clear"></div>
        </form>
	</div>
    </div>





</div>
<style>
.acc-li .hover1b{
	display:inline-block;
	}
.acc-li .hover1a{
	display:none;
	}
.acc-li .sidebar-span{
    color: #94C6F2;}
.acc-li{
	background-color:#15212d;}
</style>
<!--
<a href="adminAddReferee.php"></a>
<a href="adminCheckTrade.php"></a>
<a href="adminCheckPreviousTrade.php"></a>
<a href="adminWithdrawalRequest.php"></a>-->
<?php include 'js.php'; ?>

</body>
</html>