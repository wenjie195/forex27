<?php
class Editrecord {
    /* Member variables */
    var $id, $tradeUid, $uid, $amount, $result, $resultEdited, $editBy, 
            $dateCreated, $dateUpdated;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
        
    /**
     * @return mixed
     */
    public function getTradeUid()
    {
        return $this->tradeUid;
    }

    /**
     * @param mixed $tradeUid
     */
    public function setTradeUid($tradeUid)
    {
        $this->tradeUid = $tradeUid;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $betType
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getResultEdited()
    {
        return $this->resultEdited;
    }

    /**
     * @param mixed $resultEdited
     */
    public function setResultEdited($resultEdited)
    {
        $this->resultEdited = $resultEdited;
    }

    /**
     * @return mixed
     */
    public function getEditBy()
    {
        return $this->editBy;
    }

    /**
     * @param mixed $editBy
     */
    public function setEditBy($editBy)
    {
        $this->editBy = $editBy;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getEditRecord($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","trade_uid","uid","amount","result","result_edited","edit_by",
                                "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"edit_record");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $tradeUid, $uid, $amount, $result, $resultEdited, $editBy, 
                                $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new Editrecord;
            $user->setId($id);
            $user->setTradeUid($tradeUid);
            $user->setUid($uid);
            $user->setAmount($amount);
            $user->setResult($result);
            $user->setResultEdited($resultEdited);
            $user->setEditBy($editBy);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
