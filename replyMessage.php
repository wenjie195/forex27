<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userSMSDetails = getMessage($conn);
// $userSMSDetails = $messageRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/replyMessage.php" />
    <meta property="og:title" content="Reply Message | De Xin Guo Ji 德鑫国际" />
    <title>Reply Message | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/replyMessage.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
    <?php include 'headerAfterLogin.php'; ?>

    <h1 class="menu-distance h1-title white-text text-center">Reply Message</h1>

    <!-- <h1 class="menu-distance h1-title white-text text-center">Message ID : #<?php echo $_POST['message_uid'];?></h1> -->

    <div class="width100 overflow blue-opa-bg padding-box radius-box smaller-box">
        <form action="utilities/replyMessageTwoFunction.php" method="POST">
            <table class="shipping-table">

                <?php
                if(isset($_POST['message_uid']))
                {
                $conn = connDB();
                //Order
                $messageDetails = getMessage($conn,"WHERE message_uid = ? ", array("message_uid") ,array($_POST['message_uid']),"s");

                if($messageDetails != null)
                {
                ?>
                    <p class="input-title-p">Message : <?php echo $messageDetails[0]->getReceiveSMS()?></p>
                    <p class="input-title-p">Reply : <?php echo $messageDetails[0]->getReplySMS()?></p>
                    <div class="clear"></div>
                    <p class="input-title-p">New Message</p>
                    <input class="clean de-input" type="text" placeholder="Your Reply Message Details" id="message_details" name="message_details" required>

                    <input class="clean de-input" type="hidden" value="<?php echo $_POST['message_uid'];?>" id="message_uid" name="message_uid" readonly>
                    <button class="clean blue-button width100 small-distance small-distance-bottom" name="refereeButton">Reply</button>
                <?php
                }

                }
                ?>

            </table>
        </form>
	</div>

</div>
<?php include 'js.php'; ?>
</body>
</html>