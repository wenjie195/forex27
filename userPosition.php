<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/timezone.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
$totalForex = 0;

$conn = connDB();
$conn->close();
?>

  <table class="table-width data-table">
    <thead>
      <tr>
      <th><b><?php echo _USERDASHBOARD_INSTRUMENT ?></b></th>
	  <th><b><?php echo _USERDASHBOARD_AMOUNT ?></b></th>
      <th><b><?php echo _USERDASHBOARD_TIMELEFT ?></b></th>
      </tr>
    </thead>
    <tbody>

      <tr>
        <td id="newNumber"></td>
        <td ></td>
        <td ></td>
      </tr>
      <?php 
      $conn = connDB();
      $betStatus = getBetstatus($conn, "WHERE uid = ?", array("uid"), array($_SESSION['uid']), "s");
      if ($betStatus)
      {
        for ($cnt=0; $cnt <count($betStatus) ; $cnt++)
        {
          $timeDate = $betStatus[$cnt]->getDateCreated();
          $timeline = $betStatus[$cnt]->getTimeline();
          $time = date("Y-m-d h:i:s a", strtotime($timeDate."+".$timeline."seconds"));
          $current = date("Y-m-d h:i:s a");
          if ($time > $current && ( strtotime($time) - strtotime($current)) >= 0)
          {
            ?>
            <tr>
              <td>
                <?php echo $betStatus[$cnt]->getCurrency() ?>
              </td>
              <td>
                <?php echo "$".$betStatus[$cnt]->getAmount() ?>
              </td>
              <?php
              if (( strtotime($time) - strtotime($current)) < 10)
              {
              ?>
                <td style="color: red">
                  <?php echo ( strtotime($time) - strtotime($current))."s" ?>
                </td>
              <?php
              }
              else
              {
              ?>
                <td>
                  <?php echo ( strtotime($time) - strtotime($current))."s" ?>
                </td>
              <?php
              }
              ?>
            </tr>
            <?php
          }
        }
      }
      $conn->close();
      ?>
    </tbody>
  </table>