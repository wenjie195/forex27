<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/BetStatus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$messageValue = getMessage($conn," WHERE uid = ? AND reply_message != '' ",array("uid"),array($uid),"s");

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i:s');
$playTime = $dt->format('s');


function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" />
    <meta property="og:title" content="User Dashboard | De Xin Guo Ji 德鑫国际" />
    <title>User Dashboard | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>

    <div class="width100 overflow menu-distance small-web-width">
        <div class="right-dashboard">
        	<div class="right-details-div2">
            	<button class="blue-button clean"><?php echo _ACC_CREATION_TOPUP ?></button>  
            </div>
            	<div class="right-right-details">
                    <p class="profile-p"><?php echo _USERDASHBOARD_ACC_NO ?> : <?php echo date('mdhi', strtotime($userDetails->getDateCreated()));?><?php echo $userDetails->getId();?></p>
                    <br>
                    <p class="profile-p"><?php echo _USERDASHBOARD_BALANCE ?>: <?php echo $userDetails->getCredit();?></p>
                </div>
                
            
        </div> 
    </div>   
    <div  class="width100 overflow blue-opa-bg border-div small-distance3 tab-div small-web-width ow-ptop">
    <div class="width100 overflow tab-divdiv tab">
        <div class="w50-button">
        	<button class="tablinks clean activetab" onclick="openTab(event, 'Transaction')"><?php echo _USERDASHBOARD_TRANSACTION ?></button>
        </div>
        <div class="w50-button">
        	<button class="tablinks clean" onclick="openTab(event, 'Position')"><?php echo _USERDASHBOARD_POSITION ?></button>
        </div>
        <!--<button class="tablinks clean" onclick="openTab(event, 'Win')"><?php echo _USERDASHBOARD_WIN ?></button>
        <button class="tablinks clean" onclick="openTab(event, 'Loss')"><?php echo _USERDASHBOARD_LOSS ?></button>-->
    </div>
    <div id="Transaction" class="tabcontent activetab block"></div>
    <div id="Position" class="tabcontent"></div>

        <div id="buy-modal" class="modal-css">
            <!-- Modal content -->
            <div class="modal-content-css forgot-modal-content login-modal-content buy-modal-css">
            <span id="close" class="close-css close-buy">&times;</span>
            <h1 id="getCurrenyName" class="h1-title white-text text-center"></h1>
                <!-- <form> -->
                <form action="utilities/submitTradeFunction.php" method="POST">
                    <div id="getCurrency" class="up-bottom-border">
                    <select class="clean  de-input display-none" id="trade_type" name="trade_type" required>
                        <option value="Buy" name="Buy"><?php echo _USERDASHBOARD_BUY ?></option>
                        <option value="Sell" name="Sell"><?php echo _USERDASHBOARD_SELL ?></option>
                    </select>

                    <!-- <p class="input-top-text">Timeline</p> -->
                    <p class="input-title-p"><?php echo _USERDASHBOARD_TIMELINE ?></p>
                    <div class="toggle">
                    <!-- <input type="radio" name="sizeBy" value="100" id="sizeWeight" class="clean" /> -->
                        <input type="radio" value="30" id="sizeDimensions" name="trade_timeline" class="clean"/>
                        <label for="sizeDimensions" class="clean w30">30</label>
                        <input type="radio" value="60" id="sizeDimensions1" name="trade_timeline" class="clean"/>
                        <label for="sizeDimensions1" class="clean mid-30 w30">60</label>
                        <input type="radio" value="180" id="sizeDimensions2" name="trade_timeline" class="clean"/>
                        <label for="sizeDimensions2" class="clean margin-right0 w30">180</label>                                                                                             
                    </div> 
                    <!-- </div> -->
                    <div class="clear"></div>

                    <!-- <p class="input-top-text"><?php //echo _USERDASHBOARD_AMOUNT ?></p>
                    <button class="clean white-selection-btn" id="trade_amount" name="trade_amount" value="100">100</button>
                    <button class="clean white-selection-btn mid-30" id="trade_amount" name="trade_amount" value="300">300</button>
                    <button class="clean white-selection-btn" id="trade_amount" name="trade_amount" value="500">500</button>
                    <button class="clean white-selection-btn" id="trade_amount" name="trade_amount" value="1000">1000</button>
                    <button class="clean white-selection-btn mid-30" id="trade_amount" name="trade_amount" value="2000">2000</button>
                    <button class="clean white-selection-btn" id="trade_amount" name="trade_amount" value="3000">3000</button>
                    <button class="clean white-selection-btn width100-ow" id="trade_amount" name="trade_amount" value="5000">5000</button> -->

                    <p class="input-title-p"><?php echo _USERDASHBOARD_AMOUNT2 ?></p>
                        <div class="toggle">
                            <input type="radio" value="100" id="trade_amount1" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount1" class="clean w30">100</label>
                            <input type="radio" value="300" id="trade_amount2" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount2" class="clean mid-30 w30">300</label>
                            <input type="radio" value="500" id="trade_amount3" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount3" class="clean w30">500</label>        
                            <input type="radio" value="1000" id="trade_amount4" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount4" class="clean w30">1000</label>
                            <input type="radio" value="2000" id="trade_amount5" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount5" class="clean mid-30 w30">2000</label>
                            <input type="radio" value="3000" id="trade_amount6" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount6" class="clean w30">3000</label>    
                            <input type="radio" value="5000" id="trade_amount7" name="trade_amount" class="clean white-selection-btn width100-ow"/>
                            <label for="trade_amount7" class="clean width100-ow">5000</label>   
                        </div>

                    <input class="clean de-input" type="hidden" name="date" value="<?php echo date('Y-m-d H:i:s'); ?>" readonly>
                    <input class="clean de-input" type="hidden" id="user_uid" name="user_uid" value="<?php echo $userDetails->getUid();?>" readonly>
                    <input class="clean de-input" type="hidden" id="user_credit" name="user_credit" value="<?php echo $userDetails->getCredit();?>" readonly>
                    <input class="clean de-input" type="hidden" id="timer" name="timer" value="<?php echo $playTime ?>" readonly>

                    <div class="clear"></div>

                    <!-- <p class="input-top-text">Other Amount</p> -->
                    <p class="input-title-p"><?php echo _USERDASHBOARD_OTHERAMOUNT ?></p>
                    <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_OTHERAMOUNT2 ?>" id="trade_other_amount" name="trade_other_amount">
					<div class="clear"></div>

                    <!-- <button class="clean blue-button width100 small-distance small-distance-bottom" name="submit_trade">Submit</button> -->
                    <button class="clean blue-button width100 small-distance small-distance-bottom" name="submit_trade"><?php echo _USERDASHBOARD_SUBMIT ?></button>

                    </div>
                </form>
            </div>
        </div>

      <div id="withdraw-modal" class="modal-css">
          <!-- Modal content -->
          <div class="modal-content-css forgot-modal-content login-modal-content">
              <span class="close-css close-withdraw">&times;</span>
              <h1 class="h1-title white-text text-center"><?php echo _USERDASHBOARD_WITHDRAW ?></h1>
              <!-- <form> -->
              <form action="utilities/submitWithdrawalFunction.php" method="POST">
              <div class="up-bottom-border">
                  <p class="input-top-text"><?php echo _USERDASHBOARD_BALANCE ?></p>
                  <!-- <p class="clean de-input no-input-style">$1,000,000</p> -->
                  <p class="clean de-input no-input-style">$<?php echo $credit;?></p>
                  <p class="input-top-text"><?php echo _USERDASHBOARD_BN ?></p>
                  <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_BN ?>" id="withdrawal_bank_name" name="withdrawal_bank_name" required>
                  <p class="input-top-text"><?php echo _USERDASHBOARD_BENEFICIALNAME ?></p>
                  <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_BENEFICIALNAME ?>" id="withdrawal_bank_acc_holder" name="withdrawal_bank_acc_holder" required>
                  <p class="input-top-text"><?php echo _USERDASHBOARD_BAN ?></p>
                  <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_BAN ?>" id="withdrawal_bank_acc_number" name="withdrawal_bank_acc_number" required>
                  <p class="input-top-text"><?php echo _JS_WITHDRAW_AMOUNT ?></p>
                  <!-- <input class="clean de-input" type="number" placeholder="<?php echo _USERDASHBOARD_AMOUNT ?>" required> -->
                  <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_AMOUNT ?>"  id="withdrawal_amount" name="withdrawal_amount" required>
                    <p class="input-title-p"><?php echo _JS_COUNTRY ?></p>
                    <select class="clean  de-input" id="trade_type" name="trade_type" required>
                        <option value="Buy" name="Buy">Country 1</option>
                        <option value="Sell" name="Sell">Country 2</option>
                    </select>                  
                  
                  
              </div>

              <input type="hidden" id="withdrawal_uid" name="withdrawal_uid" value="<?php echo $userDetails->getUid();?>" readonly>
              <input type="hidden" id="withdrawal_currentcredit" name="withdrawal_currentcredit" value="<?php echo $userDetails->getCredit();?>" readonly>

              <div class="clear"></div>
                  <button class="clean blue-button width100 small-distance small-distance-bottom"><?php echo _USERDASHBOARD_WITHDRAW ?></button>
              <div class="clear"></div>
              </form>
          </div>
      </div>

      <!-- withdraw Modal -->
      <div id="customerservice-modal" class="modal-css">
          <!-- Modal content -->
          <div class="modal-content-css forgot-modal-content login-modal-content">
              <span class="close-css close-customerservice">&times;</span>
              <h1 class="h1-title white-text text-center">Customer Service</h1>
              <!-- <form action="utilities/submitWithdrawalFunction.php" method="POST"> -->
              <form action="utilities/submitCSFunction.php" method="POST">
              <div class="up-bottom-border">

                  <p class="input-top-text">Message</p>
                  <input class="clean de-input" type="text" placeholder="Your Message" id="message_details" name="message_details" required>

              </div>

              <input type="hidden" id="sender_uid" name="sender_uid" value="<?php echo $userDetails->getUid();?>" readonly>

              <div class="clear"></div>
                  <button class="clean blue-button width100 small-distance small-distance-bottom">Click To Sent</button>
              <div class="clear"></div>
              </form>
          </div>
      </div>
    </div>
</div>

<?php include 'js.php'; ?>

</body>

<script type="text/javascript">
$(document).ready(function(){
 $("#Transaction").load("userTabs.php");
    setInterval(function() {
        $("#Transaction").load("userTabs.php");
    }, 1000);
    $("#Position").load("userPosition.php");
       setInterval(function() {
           $("#Position").load("userPosition.php");
       }, 1000);
    $("#currency_name").click( function(){
      var currenyName = $(this).val();
      $("#getCurrenyName").text(currenyName);
    });
});
</script>
</html>
