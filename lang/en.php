<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "Attention");
define("_MAINJS_ENTER_USERNAME", "Please enter your username");
define("_MAINJS_ENTER_EMAIL", "Please enter your email address");
define("_MAINJS_ENTER_ICNO", "Please enter your ID number");
define("_MAINJS_SELECT_COUNTRY", "Please choose your country");
define("_MAINJS_ENTER_PHONENO", "Please enter your phone number");
//apply in all
define("_MAINJS_ALL_LOGOUT", "Logout");
//index
define("_MAINJS_INDEX_LOGIN", "Login");
define("_MAINJS_INDEX_USERNAME", "Username");
define("_MAINJS_INDEX_PASSWORD", "Password");
define("_INDEX_BANNER_H11", "Trade Forex with Convenient,");
define("_INDEX_BANNER_H12", "Earn Money with Confidence.");
define("_INDEX_BANNER_P", "Access the FX pairs across majors with us, a unified platform with excellent services.");
define("_INDEX_WHY_TRADE_WITH_US", "Why Trade FX with De Xin Guo Ji?");
define("_INDEX_EXCELLENT_EXE", "Excellent Execution");
define("_INDEX_EXCELLENT_EXE_P", "We provide higher fill-rates, less premature stop-outs and the best price available.");
define("_INDEX_AUTO", "Automated Trading");
define("_INDEX_AUTO_P", "There is no algorithmic software to change your trades. Any slippage is a result of natural market price fluctuations.");
define("_INDEX_UI", "Intuitive User-Interface");
define("_INDEX_UI_P", "We provide an easy to navigate user-interface for our customers to buy up and buy down.");
define("_INDEX_NO_DEALER", "No Dealer Intervention");
define("_INDEX_NO_DEALER_P", "We provide live, real-time prices on the FX pairs. No algorithmic software designed to push the trades to the broker’s favour.");
define("_INDEX_HOW_TRADE", "How to Trade with Us?");
define("_INDEX_STEP1", "Create a trading account by clicking the sign-up button.");
define("_INDEX_STEP2", "Fill up your personal details to complete your KYC verification.");
define("_INDEX_STEP3", "Deposit funds with us.");
define("_INDEX_STEP4", "You can start to trade once the funds are in your account.");
//adminDashboard
define("_MAINJS_ADMIND_TITLE", "Admin Dashboard");
//adminAddReferee.php
define("_MAINJS_ADDREFEREE_TITLE", "Add New User");
//userDashboard
define("_MAINJS_USERD_TITLE", "User Dashboard");
define("_USERDASHBOARD_USER", "User");
define("_USERDASHBOARD_PROFILE", "Profile Picture");
define("_USERDASHBOARD_ACC_NO", "Account No.");
define("_USERDASHBOARD_BALANCE", "Balance");
define("_USERDASHBOARD_TRANSACTION", "Transaction");
define("_USERDASHBOARD_POSITION", "Position");
define("_USERDASHBOARD_WIN", "Total Win");
define("_USERDASHBOARD_LOSS", "Total Loss");
define("_USERDASHBOARD_SEARCH", "Search");
define("_USERDASHBOARD_SELL", "Buy Down");
define("_USERDASHBOARD_BUY", "Buy Up");
define("_USERDASHBOARD_CHANGE", "Change");
define("_USERDASHBOARD_PERCENTAGE_CHANGE", "%Change");
define("_USERDASHBOARD_INSTRUMENT", "Instrument");
define("_USERDASHBOARD_AMOUNT", "Amount");
define("_USERDASHBOARD_TIMELEFT", "Time Left");
define("_USERDASHBOARD_VI_FX_TRADE", "Forex");
define("_USERDASHBOARD_WITHDRAW", "Withdraw Fund");
define("_USERDASHBOARD_VIEW_MESSAGE", "View Message");
define("_USERDASHBOARD_CUSTOMER_SERVICE", "Customer Service");
define("_USERDASHBOARD_TIMELINE", "Timeline");
define("_USERDASHBOARD_SEC", "SEC");
define("_USERDASHBOARD_OR_AMOUNT", "Or Key In Amount Here");
define("_USERDASHBOARD_BUY_DOWN", "Buy Down");
define("_USERDASHBOARD_BUY_UP", "Buy Up");
define("_USERDASHBOARD_TIME_AMOUNT", "Please Select Timeline and Amount");
define("_USERDASHBOARD_TIME", "TIMELINE");
define("_USERDASHBOARD_OTHERAMOUNT", "OTHER AMOUNT");
define("_USERDASHBOARD_SUBMIT", "SUBMIT");
define("_USERDASHBOARD_BN", "Bank Name");
define("_USERDASHBOARD_BENEFICIALNAME", "Beneficial Name");
define("_USERDASHBOARD_BAN", "Bank Account Number");
define("_USERDASHBOARD_WITHDRAWAL", "Withdraw");
define("_USERDASHBOARD_TRADE_RECORD", "Trade");
define("_USERDASHBOARD_DASHBOARD_PAGE", "Dashboard");
define("_USERDASHBOARD_LANGUAGE", "Language");
define("_USERDASHBOARD_PRODUCT", "Product");
define("_USERDASHBOARD_CURRENT_PRICE", "Price");
define("_USERDASHBOARD_AMOUNT2", "Choose an Amount");
define("_USERDASHBOARD_OTHERAMOUNT2", "Or key in other amount here");
//header
define("_HEADER_DEXINGUOJI", "De Xin Guo Ji");
define("_HEADER_LANGUAGE", "Language/语言");
define("_HEADER_LOGOUT", "Logout");
define("_HEADER_PROFILE", "Profile");
define("_HEADER_EDIT_PROFILE", "Edit Profile");
define("_HEADER_CHANGE_EMAIL", "Change Email");
define("_HEADER_CHANGE_PHONE_NO", "Change Phone No.");
define("_HEADER_CHANGE_PASSWORD", "Change Password");
define("_HEADER_SIGN_UP", "Sign Up");
define("_HEADER_LOGIN", "Login");
define("_HEADER_MESSAGE", "Message");
//JS
define("_JS_FOOTER", "@2020 De Xin Guo Ji, All Rights Reserved.");
define("_JS_LOGIN", "Login");
define("_JS_USERNAME", "Username");
define("_JS_PASSWORD", "Password");
define("_JS_FULLNAME", "Fullname");
define("_JS_NEW_PASSWORD", "New Password");
define("_JS_CURRENT_PASSWORD", "Current Password");
define("_JS_RETYPE_PASSWORD", "Retype Password");
define("_JS_RETYPE_REFERRER_NAME", "Referrer Name");
define("_JS_REMEMBER_ME", "Remember Me");
define("_JS_FORGOT_PASSWORD", "Forgot Password?");
define("_JS_FORGOT_TITLE", "Forgot Password");
define("_JS_EMAIL", "Email");
define("_JS_SIGNUP", "Sign Up");
define("_JS_FIRSTNAME", "First Name");
define("_JS_LASTNAME", "Last Name");
define("_JS_GENDER", "Gender");
define("_JS_MALE", "Male");
define("_JS_FEMALE", "Female");
define("_JS_BIRTHDAY", "Birthday");
define("_JS_COUNTRY", "Country");
define("_JS_MALAYSIA", "Malaysia");
define("_JS_SINGAPORE", "Singpore");
define("_JS_PHONE", "Phone No.");
define("_JS_REQUEST_TAC", "Request TAC");
define("_JS_TYPE", "Type");
define("_JS_SUBMIT", "Submit");
define("_JS_PLACEORDER", "Place Order");
define("_JS_WITHDRAW_AMOUNT", "Withdraw Amount");
//SIDEBAR
define("_SIDEBAR_DASHBOARD", "Dashboard");
define("_SIDEBAR_ACC_CREATION", "Account Creation");
define("_SIDEBAR_CUSTOMER_LIST", "Customer List");
define("_SIDEBAR_WITHDRAW_REQUEST", "Withdrawal Request");
define("_SIDEBAR_CURRENT_TRADE", "Current Trade");
define("_SIDEBAR_TOTAL_PROFIT", "Total Profit/Loss");
//ADMIN DASHBOARD
define("_ADMINDASHBOARD_NO_OF_WIN", "Number Of Win");
define("_ADMINDASHBOARD_TOTAL_WIN", "Total Profit");
define("_ADMINDASHBOARD_NO_OF_LOSS", "Number Of Loss");
define("_ADMINDASHBOARD_TOTAL_LOSS", "Total Loss");
define("_ADMINDASHBOARD_WITHDRAW_REQUEST", "Withdrawal Request");
//ACCOUNT CREATION
define("_ACC_CREATION_ACC_TYPE", "Account Type");
define("_ACC_CREATION_CUSTOMER", "Customer");
define("_ACC_CREATION_ADMIN", "Admin");
define("_ACC_CREATION_TOPUP", "Top Up");
//VIEW MESSAGE
define("_VIEWMESSAGE_VIEW_ALL_MESSAGE", "View All Message");
define("_VIEWMESSAGE_NO", "NO.");
define("_VIEWMESSAGE_SENT", "SENT");
define("_VIEWMESSAGE_REPLY", "REPLY");
define("_VIEWMESSAGE_DATE", "DATE");
define("_VIEWMESSAGE_MESSAGE_STATUS", "MESSAGE STATUS");
define("_VIEWMESSAGE_READ", "READ");
define("_VIEWMESSAGE_NEW_MESSAGE", "New Message");
//WITHDRAWAL
define("_WITHDRAWAL_TIME", "Request Time");
define("_WITHDRAWAL_VERIFY", "Withdrawal Status");
//ADMIN VERIFY WITHDRAWAL
define("_ADMINVERIFYWITHDRAWAL_WITHDRAWAL_NO", "Withdrawal Number : #");
define("_ADMINVERIFYWITHDRAWAL_BANK_NAME", "Bank Name");
define("_ADMINVERIFYWITHDRAWAL_WITHDRAWAL_AMOUNT", "Withdrawal Amount");
define("_ADMINVERIFYWITHDRAWAL_DATE_AND_TIME", "Date and Time");
define("_ADMINVERIFYWITHDRAWAL_REFERENCE", "Reference");
define("_ADMINVERIFYWITHDRAWAL_REJECT", "REJECT");
define("_ADMINVERIFYWITHDRAWAL_ACCEPTED", "ACCEPTED");
define("_ADMINVERIFYWITHDRAWAL_TITLE", "Verify Withdrawal");
//ADMIN WITHDRAWAL
define("_ADMINWITHDRAWAL_USERNAME", "USERNAME");
define("_ADMINWITHDRAWAL_AMOUNT", "AMOUNT");
define("_ADMINWITHDRAWAL_REQUEST_TIME", "REQUEST TIME");
define("_ADMINWITHDRAWAL_VERIFY", "VERIFY");
//ADMIN CURRENT TRADE
define("_ADMINCURRENTTRADE_UNREALIZED_GAIN", "Unrealized Gain");
define("_ADMINCURRENTTRADE_UNREALIZED_LOSS", "Unrealized Loss");
define("_ADMINCURRENTTRADE_EDITEDCT", "Edited Current Trade");
//ADMIN WITHDRAWAL
define("_ADMINWITHDRAWAL_COMPLETE", "Complete");
define("_ADMINWITHDRAWAL_REJECT", "Reject");
//ADMIN TOTAL PROFIT/LOSS
define("_ADMINTOTALPROFITLOSS_TOTAL_PROFIT_USD", "Total Profit (USD)");
define("_ADMINTOTALPROFITLOSS_TOTAL_LOSS_USD", "Total Loss (USD)");
define("_ADMINTOTALPROFITLOSS_DATE", "Date");
define("_ADMINTOTALPROFITLOSS_TIMEFRAME", "Timeframe");
define("_ADMINTOTALPROFITLOSS_CUSTOMER_NAME", "Customer Name");
define("_ADMINTOTALPROFITLOSS_CURRENCY_PAIR", "Currency Pair");
//ADMIN MEMBER
define("_ADMINMEMBER_CREDIT", "Credit");
define("_ADMINMEMBER_ADDCREDIT", "Top Up");
define("_ADMINMEMBER_TOPUPCREDIT", "Click To Top Up");
define("_ADMINMEMBER_LASTUP", "Last Updated");
define("_ADMINMEMBER_DETAILS", "Details");
define("_ADMINMEMBER_CLICKDETAILS", "Click To View Details");
define("_ADMINMEMBER_OPERATION", "Operation");
//adminUserAddCredit
define("_AUACCURRENTCREDIT_CCREDIT", "Current Credit");
define("_AUACCURRENTCREDIT_TUAMOUNT", "Top Up Amount");
define("_AUACCURRENTCREDIT_SUBMIT", "Submit");
//adminUserAddCredit
define("_AUD_CUSDETAILA", "Customer Details");
define("_AUD_DEPOSITHIS", "Top Up  History");
define("_AUD_ACCOUNTNO", "Account No.");
define("_AUD_AMOUNT", "Amount");
define("_AUD_TOPUP", "Top Up");
define("_AUD_CLICKTOPUP", "Click To Top Up");
define("_AUD_ACCSTATUS", "Account Status");
define("_AUD_ACTIVE", "Activated");
define("_AUD_INACTIVE", "Deactivated");
define("_AUD_TOPUPBY", "Top Up By");
define("_AUD_DATE", "Date");
//adminUserTradeDetails
define("_AUTD_NOOFWIN", "No. of Win");
define("_AUTD_NOOFLOSE", "No. of Lose");
define("_AUTD_NEETGL", "Nett Gain / Lose");
define("_AUTD_WIN", "Win");
define("_AUTD_LOSE", "Lose");
define("_AUTD_TOPUP", "Top Up");
define("_AUTD_WITHDRAWAL", "Withdrawal");
//Profile
define("_PROFILE_PERSONAL_DETAILS", "Personal Details");
define("_PROFILE_TOPUP_HISTORY", "Top Up History");
define("_PROFILE_WITHDRAW_HISTORY", "Withdraw History");
//Top Up History
define("_TOPUP_HISTORY_DATE", "Date");