<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userSMSDetails = getMessage($conn,"ORDER BY date_created DESC");
// $userSMSDetails = $messageRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminViewMessage.php" />
    <meta property="og:title" content="View All Message | De Xin Guo Ji 德鑫国际" />
    <title>View All Message | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminViewMessage.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
    <h1 class="menu-distance h1-title white-text text-center">View All Message</h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box">
    <table class="table-width data-table message-table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th class="two-white-border">SENT</th>
                            <th class="two-white-border">REPLY</th>
                            <th class="two-white-border">DATE</th>
                            <th>REPLY</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($userSMSDetails)
                        {   
                            for($cnt = 0;$cnt < count($userSMSDetails) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $userSMSDetails[$cnt]->getReceiveSMS();?></td>
                                <td><?php echo $userSMSDetails[$cnt]->getReplySMS();?></td>
                                <td><?php echo $userSMSDetails[$cnt]->getDateCreated();?></td>

                                <td>
                                    <form action="adminReplyMessage.php" method="POST">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="message_uid" value="<?php echo $userSMSDetails[$cnt]->getMessageUid();?>">
                                            UPDATE
                                        </button>
                                    </form>
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
    </div>
    </div>
</div>
<?php include 'js.php'; ?>
</body>
</html>