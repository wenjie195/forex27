<div class="width100 menu-distance">
    <div class="width100 overflow profile-details-div">
    	<div class="user-left-div">
        	<div class="profile-div">
            	<img src="img/profile.jpg" class="profile-pic2 width100" alt="<?php echo _USERDASHBOARD_PROFILE ?>" title="<?php echo _USERDASHBOARD_PROFILE ?>">
            </div>
            <div class="profile-info-div">
            	<p class="profile-p">
                	<?php echo _USERDASHBOARD_ACC_NO ?> : <?php echo date('mdhi', strtotime($userDetails->getDateCreated()));?><?php echo $userDetails->getId();?>
                </p>
                <p class="profile-p"><?php echo _USERDASHBOARD_BALANCE ?>: <?php echo $userDetails->getCredit();?></p>
            </div>
        </div>
    	<div class="user-right-div">
        	<img src="img/logo3.png" alt="De Xin Guo Ji 德鑫国际" title="De Xin Guo Ji 德鑫国际" class="logo2-img web-logo1">
            <img src="img/logo2.png" alt="De Xin Guo Ji 德鑫国际" title="De Xin Guo Ji 德鑫国际" class="logo2-img mobile-logo1">
        </div>
    </div>
    <div class="clear"></div>
    <div class="width100 extra-m-btm small-distance x-distance">
        <div class="blue-button open-withdraw text-center w30 float-left profile-3-btn ow-green-bg"><?php echo _USERDASHBOARD_WITHDRAW ?></div>

        <!-- <a href="#" class="inline-block w30 mid-30 float-left profile-3-btn"></a> -->

        <div class="blue-button open-customerservice text-center w30 mid-30 float-left profile-3-btn ow-blue-bg"><?php echo _USERDASHBOARD_CUSTOMER_SERVICE ?></div>

        <form action="utilities/requestTopUpFunction.php" method="POST">
            <?php
                if($userRows)
                {   
                ?>
                    <button class="blue-button open-withdraw text-center w30 float-left clean profile-3-btn ow-gold-bg" name="usertopup_id" value="<?php echo $userDetails->getUid();?>">
                        <?php echo _ADMINMEMBER_ADDCREDIT ?>
                    </button>
                <?php
                }
            ?>
        </form>

    </div>
</div>

<div class="clear"></div>