<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created DESC",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/viewMessage.php" />
    <meta property="og:title" content="View Message | De Xin Guo Ji 德鑫国际" />
    <title>View Message | De Xin Guo Ji 德鑫国际</title>
    <meta property="og:url" content="https://dxforextrade88.com/viewMessage.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>
    <h1 class="menu-distance h1-title white-text text-center"><?php echo _VIEWMESSAGE_VIEW_ALL_MESSAGE ?></h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box">
    <table class="table-width data-table message-table">
                    <thead>
                        <tr>
                            <th><?php echo _VIEWMESSAGE_NO ?></th>
                            <th class="two-white-border"><?php echo _VIEWMESSAGE_SENT ?></th>
                            <th class="two-white-border"><?php echo _VIEWMESSAGE_REPLY ?></th>
                            <th><?php echo _VIEWMESSAGE_DATE ?></th>
                            <th><?php echo _VIEWMESSAGE_MESSAGE_STATUS ?></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($userSMSDetails)
                        {   
                            for($cnt = 0;$cnt < count($userSMSDetails) ;$cnt++)
                            {
                            ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $userSMSDetails[$cnt]->getReceiveSMS();?></td>
                                <td><?php echo $userSMSDetails[$cnt]->getReplySMS();?></td>
                                <td><?php echo $userSMSDetails[$cnt]->getDateCreated();?></td>

                                <td>
                                    <?php
                                    $conn = connDB();
                                    $messageUserStatus = $userSMSDetails[$cnt]->getUserStatus();
                                    $messageAdminStatus = $userSMSDetails[$cnt]->getAdminStatus();
                                    // if($messageStatus != 'READ')
                                    if($messageUserStatus == 'READ')
                                    {
                                    ?>
                                        <p class="read-color">READ</p>
                                    <?php
                                    }
                                    elseif($messageAdminStatus == 'REPLY')
                                    {
                                    ?>
                                        <form action="utilities/updateMessageStatus.php" method="POST">
                                            <button class="clean blue-button smaller-font" type="submit" name="message_uid" value="<?php echo $userSMSDetails[$cnt]->getMessageUid();?>">
                                                <?php echo _VIEWMESSAGE_NEW_MESSAGE ?>
                                            </button>
                                        </form>
                                    <?php
                                    }
                                    // else
                                    elseif($messageAdminStatus == 'GET')
                                    {
                                    ?>
                                        <p class="read-color">PENDING</p>
                                    <?php
                                    }
                                    $conn->close();
                                    ?>
                                </td>
                            <?php
                            }
                            ?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
	</div>
</div>
<?php include 'js.php'; ?>
</body>
</html>